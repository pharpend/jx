# jx: secure TypeScript package manager

## How to use

1. You need Python 3.6 or later
2. Put the `jx` script in your `$PATH`

## Cheat sheet

- linter: `pyflakes jx` (checks for things like unused variables or calling a function with the wrong number of arguments)
- type checker: `mypy jx` (dialyzer.py)

Need to `pip3 install pyflakes mypy`

The chimp version of pyflakes installed on Ubuntu 18.04 doesn't know about type
annotation syntax. So make sure you're using the version `pip3` installed.

## Motivation

I have two projects

1.  Jaeck Russell: a browser extension
2.  Sidekick: a script that commercial users serve in their webpages that talks
    to Jaeck Russell

They are obviously separate projects. But also there is a fair amount of code
reuse between the two, particularly type definitions.

So it's natural to factor it out into three packages, roughly with this
dependency graph

```
awcp
|
---- sidekick
|  |
|  ---- sidekick_examples
|
---- jrx
```

What I would like is a tool that automates "I changed AWCP and it broke
sidekick, and I would like to hear about it."

More commonly, I break sidekick and it breaks one of the examples.

I previously have just been using `ln -s` to do this, but that scheme is
becoming too cumbersome.

## NPM/Browserify

NPM solves this problem with a runtime layer. So the project structure would be
something like

```
awcp/
    package.json
    src/
        awcp.ts
jr/
    package.json
    node_modules/
        awcp/
    src/
        jr.ts
sidekick/
    package.json
    node_modules/
        awcp/
    examples/
    src/
        sidekick.ts
sidekick_examples/
    package.json
    node_modules/
        awcp/
        sidekick/
    src/
        foo.ts
        bar.ts
        baz.ts
```

This is certainly an attractive directory structure. To import across projects,
I would just have, say, in `sidekick/src/sidekick.ts`

```js
const awcp = require('awcp');
```

`require()` is a Node primitive that is specific to the Node runtime and
doesn't work in the browser. NPM handles the task of making sure all of the
dependencies end up in the right place.

In the browser, we have to do something like

```js
import * as awcp from './path/to/awcp.js'
```

There are tools (namely [Browserify](https://browserify.org/)) which can
automate the process of rewriting `require`s as `import`s.

From [the "how browserify works" section of the handbook](https://github.com/browserify/browserify-handbook#how-browserify-works)

> Browserify starts at the entry point files that you give it and searches for any
> `require()` calls it finds using
> [static analysis](http://npmjs.org/package/detective)
> of the source code's
> [abstract syntax tree](https://en.wikipedia.org/wiki/Abstract_syntax_tree).
>
> For every `require()` call with a string in it, browserify resolves those module
> strings to file paths and then searches those file paths for `require()` calls
> recursively until the entire dependency graph is visited.
>
> Each file is concatenated into a single javascript file with a minimal
> `require()` definition that maps the statically-resolved names to internal IDs.
>
> This means that the bundle you generate is completely self-contained and has
> everything your application needs to work with a pretty negligible overhead.
>
> Note: If your `require()` contains anything other than a string literal (i.e. a variable) then it cannot be read at time of bundling, so the module being required will not be concatenated into your bundle and likely cause a runtime error.
>
> For more details about how browserify works, check out the compiler pipeline
> section of this document.

### The problem with Node/NPM/Browserify

The basic problem is that I am developing software that handles real money for
real e-commerce.

1. NPM is not a trustworthy tool
2. Therefore anything installed using NPM is untrustworthy (e.g. tsc, minifiers, webpack, browserify)
3. Tools that do opaque rewrites (e.g. minifiers, webpack, browserify) add an additional layer of potential security issues

`tsc` is sort of an exception.  Overall, it improves security by dramatically
increasing the code quality and catching tons of potential errors.  Crucially,
`tsc` generates human-readable output that corresponds to the source code in a
very straightforward way.

## What I want

I want a tool similar to [ZX/Zomp](http://zxq9.com/projects/zomp/), but for JS
builds.

Constraints:

1. No opaque rewrites
2. No runtime layer

Shoulds:

1. Just Work™
2. Compose nicely
3. No remote fetches

zomp is quite sophisticated and has all sorts of crazy networking features I
don't understand.

Assumptions:

1.  Don't have tons of JS code (so, for instance, serving a file tree isn't a
    bottleneck, don't need minifiers/bundlers).

    The whole tree for sidekick (non-minified, comments not removed, sources
    included) is like 200K, which is the size of a modest JPEG file.
    Especially if you consider that caching is a thing, serving this as a tree
    is simply a non-issue.


## Attempt 0: What I was doing before

I started off *just* developing sidekick, and then later have started doing JR.

Sidekick already has a mini version of this problem which is that it has tons
of "example" code.

So the structure was roughly

```
sidekick/
    tsconfig.json
    examples/
        tsconfig.json
        index.html
        example_a.html
        example_b.html
        example_c.html
        dist/
            example_a.js
            example_a.js.map
            example_a.ts.d
            example_b.js
            example_b.js.map
            example_b.ts.d
            example_c.js
            example_c.js.map
            example_c.ts.d
        src/
            example_a.ts
            example_b.ts
            example_c.ts
    dist/
        foo.js
        foo.js.map
        foo.ts.d
        bar.js
        bar.js.map
        bar.ts.d
        baz.js
        baz.js.map
        baz.ts.d
    src/
        foo.ts
        bar.ts
        baz.ts
```

The basic problem that is relevant here is that the `examples` code depends on
`foo.ts`, `bar.ts` and `baz.ts`. So there has to be a way to make those source
files visible to `tsc` running inside `sidekick/examples` (notice
`examples/tsconfig.json`).

For releases, my idea was to, instead of publishing an NPM package, just make a
tarball containing the `src` and `dist` directories (along with things like the
`README` and `LICENSE`).

There was a `make` target that generated a "release tarball", something like

```
sidekick-0.1.0/
    README.md
    LICENSE
    dist/
        foo.js
        foo.js.map
        foo.ts.d
        bar.js
        bar.js.map
        bar.ts.d
        baz.js
        baz.js.map
        baz.ts.d
    src/
        foo.ts
        bar.ts
        baz.ts
```

The idea here is that a commercial user of sidekick can just grab this tarball,
drop it inside their static tree, and just serve that.

It's worth keeping in mind that, in the examples, I am trying to illustrate the
way I intend for you to use sidekick. So doing this is no:

```ts
// file: sidekick/examples/src/example_a.ts
import * as foo from '../../dist/foo.js'
```

This "self contained release tarball" idea is a good idea.  We get
human-readable scripts with proper debug symbols that Just Work™.  But it
doesn't *quite* translate into this "dependency" idiom, as you'll see in the
next section.

So to illustrate the "correct" way to do this, I was symlinking
`sidekick/examples/sidekick-0.1.0 -> sidekick-0.1.0`. Not really ideal

Also I wasn't even using correct versioning, I was using `sidekick_mindist`
instead of `sidekick-0.1.0`.

So the full structure would be something like

```
sidekick/
    examples/
        index.html
        example_a.html
        example_b.html
        example_c.html
        sidekick-0.1.0 -> ../sidekick-0.1.0
        dist/
            example_a.js
            example_a.js.map
            example_a.ts.d
            example_b.js
            example_b.js.map
            example_b.ts.d
            example_c.js
            example_c.js.map
            example_c.ts.d
        src/
            example_a.ts
            example_b.ts
            example_c.ts
    sidekick-0.1.0/
        README.md
        LICENSE
        dist/
            foo.js
            foo.js.map
            foo.ts.d
            bar.js
            bar.js.map
            bar.ts.d
            baz.js
            baz.js.map
            baz.ts.d
        src/
            foo.ts
            bar.ts
            baz.ts
    dist/
        foo.js
        foo.js.map
        foo.ts.d
        bar.js
        bar.js.map
        bar.ts.d
        baz.js
        baz.js.map
        baz.ts.d
    src/
        foo.ts
        bar.ts
        baz.ts
```

This is very annoying. It is hard to automate the builds, the builds were very
complicated, and not reproducible (depended on doing a bunch of symlink jizz).

So, just in this case, I want to automate "make a distribution of sidekick and
use it in the examples."

I need to typecheck my examples because I break them a lot.

## Attempt 1: Let's make `rsync++`

Alright, my first idea was to just take that "distribution tarball" and write a
tool that just stores the distribution tree in a special location.

```
// file: sidekick/jx.json
{"realm"   : "ppr",
 "name"    : "sidekick",
 "version" : "0.1.0",
 "deps"    : []}
```

```
~/vanillae/sidekick $ make jx_mindist
tsc
mkdir -p jx_mindist
cp    README.md   jx_mindist
cp    LICENSE     jx_mindist
cp    jx.json     jx_mindist
cp -r src         jx_mindist
cp -r dist        jx_mindist
~/vanillae/sidekick $ jx push -f
copying /home/pharpend/vanillae/sidekick/jx_mindist to /home/pharpend/.jx/dev/ppr-sidekick-0.1.0
copied /home/pharpend/vanillae/sidekick/jx_mindist to /home/pharpend/.jx/dev/ppr-sidekick-0.1.0
```

```
// file: sidekick/examples/jx.json
{"realm"   : "ppr",
 "name"    : "sidekick_examples",
 "version" : "0.1.0",
 "deps"    : ["ppr-sidekick-0.1.0"]}
```

```
~/vanillae/sidekick/examples $ jx pull -f
pulling ppr-sidekick-0.1.0
source /home/pharpend/.jx/dev/ppr-sidekick-0.1.0
destination /home/pharpend/vanillae/sidekick/examples/./src/jx_include/ppr-sidekick-0.1.0
mkdir -p /home/pharpend/vanillae/sidekick/examples/./src/jx_include/ppr-sidekick-0.1.0
maybe_force_copytree force=True src=/home/pharpend/.jx/dev/ppr-sidekick-0.1.0 dst=/home/pharpend/vanillae/sidekick/examples/./src/jx_include/ppr-sidekick-0.1.0
copying /home/pharpend/.jx/dev/ppr-sidekick-0.1.0 to /home/pharpend/vanillae/sidekick/examples/./src/jx_include/ppr-sidekick-0.1.0
copied /home/pharpend/.jx/dev/ppr-sidekick-0.1.0 to /home/pharpend/vanillae/sidekick/examples/./src/jx_include/ppr-sidekick-0.1.0
~/vanillae/sidekick/examples $ tree
.
├── hello.html
├── index.html
├── jx.json
├── logging.html
├── logging_http.html
├── src
│   ├── hello.ts
│   ├── jx_include
│   │   └── ppr-sidekick-0.1.0
│   │       ├── dist
│   │       │   ├── awcp.d.ts
│   │       │   ├── awcp.js
│   │       │   ├── awcp.js.map
│   │       │   ├── sidekick.d.ts
│   │       │   ├── sidekick.js
│   │       │   └── sidekick.js.map
│   │       ├── jx.json
│   │       ├── LICENSE
│   │       ├── README.md
│   │       └── src
│   │           ├── awcp.ts
│   │           └── sidekick.ts
│   ├── logging_http.ts
│   └── logging.ts
└── tsconfig.json

5 directories, 20 files
```

And to use sidekick from one of the examples:

```typescript
// file: sidekick/examples/src/hello.ts
import * as sk from './jx_include/ppr-sidekick-0.1.0/dist/sidekick.js';

sk.hello();
```

Great! This tool seems to be automating everything, and further, it's
exhibiting how to use releases properly!

Do you see the problem?

No?

Let's run `tsc` and look at the output of `tree` again:

```
~/vanillae/sidekick/examples $ tsc
~/vanillae/sidekick/examples $ tree
.
├── dist
│   ├── hello.d.ts
│   ├── hello.js
│   ├── hello.js.map
│   ├── jx_include
│   │   └── ppr-sidekick-0.1.0
│   │       └── src
│   │           ├── awcp.d.ts
│   │           ├── awcp.js
│   │           ├── awcp.js.map
│   │           ├── sidekick.d.ts
│   │           ├── sidekick.js
│   │           └── sidekick.js.map
│   ├── logging.d.ts
│   ├── logging_http.d.ts
│   ├── logging_http.js
│   ├── logging_http.js.map
│   ├── logging.js
│   └── logging.js.map
├── hello.html
├── index.html
├── jx.json
├── logging.html
├── logging_http.html
├── src
│   ├── hello.ts
│   ├── jx_include
│   │   └── ppr-sidekick-0.1.0
│   │       ├── dist
│   │       │   ├── awcp.d.ts
│   │       │   ├── awcp.js
│   │       │   ├── awcp.js.map
│   │       │   ├── sidekick.d.ts
│   │       │   ├── sidekick.js
│   │       │   └── sidekick.js.map
│   │       ├── jx.json
│   │       ├── LICENSE
│   │       ├── README.md
│   │       └── src
│   │           ├── awcp.ts
│   │           └── sidekick.ts
│   ├── logging_http.ts
│   └── logging.ts
└── tsconfig.json

9 directories, 35 files
```

Do you see the problem now?

No?

Ok, let's open `hello.html` in a browser

Here's `hello.html` for reference

    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="utf-8">
        <title>Sidekick Example: Hello World</title>
    </head>
    <body>

    <!-- Link tree -->
    <ul><li>
        <a href="../">Examples</a>
        <ul><li>
            <b>Hello world</b>
        </li></ul>
    </li></ul>


    <h1>Sidekick Example: Hello World</h1>

    <h4>Check the console</h4>

    <p>
        This example just demonstrates how to import sidekick and call functions
        from the module. Useful for debugging purposes, or getting out of mental
        quicksand.
    </p>


    <!-- script -->
    <script type="module" src="./dist/hello.js"></script>
    </body>
    </html>

So we're loading the JS from `sidekick/examples/dist/hello.js`, i.e. the output
of running `tsc` in the `sidekick/examples/` directory.

Let's spin up an HTTP server (need to use a server because something about how
modules work, I dunno) and try opening this in Firefox and see what happens

    ~/vanillae/sidekick/examples $ python3 -m http.server 8001
    Serving HTTP on 0.0.0.0 port 8001 (http://0.0.0.0:8001/) ...
    127.0.0.1 - - [31/Jul/2022 13:24:55] "GET / HTTP/1.1" 200 -
    127.0.0.1 - - [31/Jul/2022 13:24:55] code 404, message File not found
    127.0.0.1 - - [31/Jul/2022 13:24:55] "GET /favicon.ico HTTP/1.1" 404 -
    127.0.0.1 - - [31/Jul/2022 13:24:59] "GET /hello.html HTTP/1.1" 200 -
    127.0.0.1 - - [31/Jul/2022 13:24:59] "GET /dist/hello.js HTTP/1.1" 200 -
    127.0.0.1 - - [31/Jul/2022 13:24:59] code 404, message File not found
    127.0.0.1 - - [31/Jul/2022 13:24:59] "GET /dist/jx_include/ppr-sidekick-0.1.0/dist/sidekick.js HTTP/1.1" 404 -

Oops.

See, I was hoping that `tsc` would copy the file tree correctly. But it
doesn't. `tsc` just recurses the `src` directory, sees some TypeScript in
`sidekick/examples/src/jx_include/src/`, and just compiles it, preserving the
directory structure, and *ignoring the JS* in
`sidekick/examples/src/jx_include/dist/`.  You can see the problem directly
if we compare the tree of
`sidekick/examples/dist/jx_include/ppr-sidekick-0.1.0` to the tree of
`sidekick/examples/src/jx_include/ppr-sidekick-0.1.0/`

    ~/vanillae/sidekick/examples $ tree dist/jx_include/ppr-sidekick-0.1.0
    dist/jx_include/ppr-sidekick-0.1.0
    └── src
        ├── awcp.d.ts
        ├── awcp.js
        ├── awcp.js.map
        ├── sidekick.d.ts
        ├── sidekick.js
        └── sidekick.js.map

    1 directory, 6 files
    ~/vanillae/sidekick/examples $ tree src/jx_include/ppr-sidekick-0.1.0
    src/jx_include/ppr-sidekick-0.1.0
    ├── dist
    │   ├── awcp.d.ts
    │   ├── awcp.js
    │   ├── awcp.js.map
    │   ├── sidekick.d.ts
    │   ├── sidekick.js
    │   └── sidekick.js.map
    ├── jx.json
    ├── LICENSE
    ├── README.md
    └── src
        ├── awcp.ts
        └── sidekick.ts

    2 directories, 11 files

It seems that my naive "let's make `rsync++` approach has reached its limit.

Up to this point, all `jx` does is copy files around.

Probably, `jx` is going to need to know how to build things.

Looking through the `tsconfig` documentation, I think the best option is to
just use an `exclude` field in `tsconfig.json`, and have `jx` handle that part
of the copying.

The other option is to separate `source` and `distribution` tarballs, which
might not be a bad idea.  And so for a library dependency, `jx` would pull in
just the sources, so it would be something like

    sidekick/
        examples/
            tsconfig.json
            index.html
            hello.html
            src/
                hello.ts
                jx_include/
                    ppr-sidekick-0.1.0-src/
                        awcp.ts
                        sidekick.ts
            dist/
                hello.js
                hello.js.map
                jx_include/
                    ppr-sidekick-0.1.0-src/
                        awcp.js
                        awcp.js.map
                        sidekick.js
                        sidekick.js.map

The problem we're likely to run into here is that TypeScript's type checks are
not a binary thing.  It's more like there's a whole spectrum of potential
things you can configure in `tsconfig.json`.  There's tons of options that you
can turn up or down depending on how strict you want `tsc` to be.  So we can't
just drop TypeScript-that-compiles-in-project-A into project B and expect it to
work with project B's settings.

And this is just begging for bubbling errors.

Better to have each project build separately, and typecheck against completed
builds.

So I think using `exclude` is a better option.

However, this means that `jx` needs to have some knowledge of the build
process.  And I'm debating how expansive I want to make the scope of the tool.

So, lots to think about.

## Attempt 2: Real build system

Should probably do first:

- figure out logging

Steps

1. Dependency clone into `src/jx_include/`
2. Build using `tsc` (assume `jx_include` is ignored by `tsc`)
3. `mkdir .jx_mindist`
4. Copy `src` and `dist` (along with `README.md` and `LICENSE`) to `.jx_mindist`
5. Dependency clone into `.jx_mindist/dist/jx_include/`
6. Push tarball to `~/.jx/dev/ppr-sidekick-0.1.0.tar.gz`

Tempted to distinguish between "mindist" and "fulldist". The fulldist would
include all of the examples and documentation.

Hmm... there should also be some metadata contained in the tarball so `jx`
knows how to include it... thought.

Will eventually need `license` fields, etc.

Keep it simple for now

One idea I had was to do this:

```json
{"realm"   : "ppr",
 "name"    : "sidekick",
 "version" : "0.1.0",
 "deps"    : [],
 "make"    : {"all"      : {"prereqs" : ["mindist"],
                            "script"  : []},
              "tsc"      : {"prereqs" : [],
                            "script"  : ["tsc"]},
              "mindist"  : {"prereqs" : ["tsc"],
                            "script"  : ["mkdir -p jx_mindist",
                                         "cp    README.md jx_mindist",
                                         "cp    LICENSE   jx_mindist",
                                         "cp    jx.json   jx_mindist",
                                         "cp -r src       jx_mindist",
                                         "cp -r dist      jx_mindist"]}}}
```

But no, that's too stupid and overengineered

Step 1 is to find an example of Python logging

This is pretty good: https://docs.python.org/3.6/howto/logging-cookbook.html

This is what I want: https://docs.python.org/3.6/howto/logging-cookbook.html#multiple-handlers-and-formatters

Also should have a feature to validate the `jx.json` file.

Next thing I should do is type annotations, because the program is getting complicated

Then `jx.json` validator

Should have a `validate` command

Alright, did type annotations

Next thing is to have a proper `build` command

Then update `push` so that it pushes tarballs

Then add a `get` command

Then add a `clone` command

Change `pull` to `clone-deps`

hmmm, I'm overengineering I fear, but I think I'm doing the right level of overengineering.

I need a break

Alright

almost got it down

I need to separate out `jx build` and `jx mindist`
